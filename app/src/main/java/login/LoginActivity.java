package login;

import android.app.ProgressDialog;
import android.com.ideasharingapp.AppController;
import android.com.ideasharingapp.IdeaListActivity;
import android.com.ideasharingapp.R;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import iconstant.Iconstant;

/**
 * Created by insonix on 22/4/16.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, Iconstant {
    Button register;
    ProgressDialog progressDialog;
    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    String url;
    EditText name,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        register=(Button)findViewById(R.id.login);
        register.setOnClickListener(this);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor=sharedpreferences.edit();
        progressDialog=new ProgressDialog(LoginActivity.this);
        name=(EditText)findViewById(R.id.user_name);
        email=(EditText)findViewById(R.id.email);

    }

    @Override
    public void onClick(View v) {
     switch (v.getId()){
         case R.id.login:

             login_Process();
             break;
     }
    }
    public void login_Process(){

        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();



         url = "http://demo.insonix.com/bush/login.php?name=admin&email=admin@anheuser-busch.com";
        Log.d("url:", "urllogin" + url);



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());

                try {
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("true")) {
                       JSONObject response=jsonObject.getJSONObject("response");

                        String email = response.getString("email");
                        String username = response.getString("name");
                        String uid = response.getString("id");


                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();


                        editor.putString(uUsername, username);

                        editor.putString(uEmail, email);
                        editor.putString(uUid, uid);



                        editor.commit();


                        Intent intent = new Intent(LoginActivity.this, IdeaListActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);

                    } else {
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                } catch (Exception e) {
                   e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}
