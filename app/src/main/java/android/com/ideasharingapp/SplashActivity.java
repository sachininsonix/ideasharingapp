package android.com.ideasharingapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import login.LoginActivity;

/**
 * Created by insonix on 22/4/16.
 */
public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);


        new Thread(new Runnable() {
            @Override
            public void run() {


                try{
                  Thread.sleep(3000);
                    Intent intent=new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                }
            }
        }).start();
    }
}
