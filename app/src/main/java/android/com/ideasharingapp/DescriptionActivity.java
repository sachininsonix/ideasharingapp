package android.com.ideasharingapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import iconstant.Iconstant;

/**
 * Created by insonix on 22/4/16.
 */
public class DescriptionActivity extends Activity implements Iconstant {
    ImageView menu,btn_comment,img_like;
    ListView mybuy;
    LinearLayout lin_like;
    IdeaAdapterComment ideaAdapter;
    TextView title,description,likes,nocomment,text_like;
    EditText comment;
    SharedPreferences sharedPreferences;
    String url,comments,url2,url1,url3,like_response;
    ArrayList<HashMap<String,String>> addme;
    ProgressDialog progressDialog;
    HashMap map;
    int count;

    Intent intent;
    String sug_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idea_detail);
        progressDialog=new ProgressDialog(this);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        addme=new ArrayList<HashMap<String, String>>();
        menu=(ImageView)findViewById(R.id.menu);
        img_like=(ImageView)findViewById(R.id.img_like);
        btn_comment=(ImageView)findViewById(R.id.btn_comment);
        lin_like=(LinearLayout)findViewById(R.id.lin_like);
        mybuy=(ListView)findViewById(R.id.mybuy);
        title=(TextView)findViewById(R.id.title);
        text_like=(TextView)findViewById(R.id.text_like);
        nocomment=(TextView)findViewById(R.id.nocomment);
        likes=(TextView)findViewById(R.id.likes);
        description=(TextView)findViewById(R.id.description);
        comment=(EditText)findViewById(R.id.comment);
//        comments=comment.getText().toString();
        intent=getIntent();
//        title.setText(intent.getStringExtra("title"));
//        description.setText(intent.getStringExtra("idea_des"));
        try{
        sug_id=intent.getStringExtra("id");}
        catch (Exception e){

        }
        //commentsjson();
        getsinglesug();

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  addme=new ArrayList<HashMap<String, String>>();
               // comments= String.valueOf(comment.getText());

                commentsjson();

            }
        });
       /* commentsjson();
        commentjsonget();
*/
        lin_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(like_response.equals("0")){
                    count=1;
                }else{
                    count=0;
                }
                clicklike();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DescriptionActivity.this,IdeaListActivity.class);
                startActivity(intent);
                finish();


            }
        });

    }
    void commentsjson(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url1="http://demo.insonix.com/bush/add_comment.php?comments="+comment.getText().toString().trim()+"&user_id=1&sug_id="+intent.getStringExtra("id");
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("commnt", String.valueOf(jsonObject));
                comment.setText("");
                commentjsonget();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    void clicklike(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url3="http://demo.insonix.com/bush/add_likes.php?sug_id="+sug_id+"&user_id="+sharedPreferences.getString(uUid,"")+"&like_value="+count;
        Log.d("url3", url3);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url3, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("commnt", String.valueOf(jsonObject));
                try{
                   if(like_response.equals("0")){
                       text_like.setTextColor(Color.parseColor("#000000"));
                       img_like.setBackgroundResource(R.drawable.ic_thumb_up_24dp);
                   }else{
                       img_like.setBackgroundResource(R.drawable.like);
                       text_like.setTextColor(Color.parseColor("#f37022"));
                   }
                    getsinglesug();
                }catch (Exception e){

                }

                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    void getsinglesug(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url2="http://demo.insonix.com/iocl/get_single_sug.php?sug_id="+sug_id+"&user_id="+sharedPreferences.getString(uUid,"");
        Log.d("commnt", url2);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
               try{
                   Log.d("commnt", jsonObject.toString());
                  String status=jsonObject.getString("status");
                   if(status.equalsIgnoreCase("true")){
                       JSONObject response=jsonObject.getJSONObject("response");
                       String id=response.getString("id");
                       String title1=response.getString("title");
                       String type_of_idea=response.getString("type_of_idea");
                       String idea_des=response.getString("idea_des");
                       String file_url1=response.getString("file_url1");
                        like_response=response.getString("like_response");
                       String likes1=response.getString("likes");
                       title.setText(title1);
                       likes.setText(likes1);
                       description.setText(idea_des);
                       if(like_response.equals("0")){
                           text_like.setTextColor(Color.parseColor("#000000"));
                           img_like.setBackgroundResource(R.drawable.ic_thumb_up_24dp);
                       }else{
                           img_like.setBackgroundResource(R.drawable.like);
                           text_like.setTextColor(Color.parseColor("#f37022"));
                       }
                       commentjsonget();

                   }
               }catch(Exception e){
                  e.printStackTrace();
                }

                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest,"tag_request");
    }
    void commentjsonget(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url="http://demo.insonix.com/iocl/get_comments.php?sug_id="+sug_id;
        JsonObjectRequest jsonObjectRequestget=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                addme.clear();

                try {
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            map = new HashMap();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String comments = jsonObject1.getString("comments");
                            String added = jsonObject1.getString("added");
                            map.put("comments", comments);
                            map.put("added", added);
                            addme.add(map);
                        }
                        mybuy.setVisibility(View.VISIBLE);
                        nocomment.setVisibility(View.GONE);
                        ideaAdapter = new IdeaAdapterComment(DescriptionActivity.this);
                        mybuy.setAdapter(ideaAdapter);
                    }else{
                        mybuy.setVisibility(View.GONE);
                        nocomment.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequestget);

    }
    class IdeaAdapterComment extends BaseAdapter {
        Activity activity;

        public IdeaAdapterComment(Activity activity){
            this.activity=activity;

        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.comment_item,null);
            TextView comments_item=(TextView)convertView.findViewById(R.id.comments_item);
            TextView added=(TextView)convertView.findViewById(R.id.date);
            comments_item.setText(addme.get(position).get("comments"));
            String split[]=addme.get(position).get("added").split(" ");
            String time[]=split[1].split(":");
            added.setText(time[0]+":"+time[1]);
            Log.d("datee",split[1]);
            return convertView;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(DescriptionActivity.this,IdeaListActivity.class);
        startActivity(intent);
        finish();
    }
}
