package android.com.ideasharingapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import iconstant.Iconstant;
import iconstant.ShowMsg;
import login.LoginActivity;

/**
 * Created by insonix on 22/4/16.
 */
public class IdeaListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Iconstant {
    ListView recyclerView;
    IdeaAdapter ideaAdapter;
    LinearLayout focus_1,trend_1;
    ImageView add_icon,menu;
    TextView focus,trend;
    View view1,view2;
    String url;
    ArrayList<HashMap<String,String >> addme;
    ProgressDialog progressDialog;
    Spinner spinner;
    SharedPreferences sharedpreferences;
    ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        progressDialog=new ProgressDialog(IdeaListActivity.this);
        addme=new ArrayList<>();
imageLoader= ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(IdeaListActivity.this));
        displayImageOptions=new DisplayImageOptions.Builder().showStubImage(R.drawable.add_user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        recyclerView=(ListView)findViewById(R.id.recycler);
        add_icon=(ImageView)findViewById(R.id.add_icon);
        trend=(TextView)findViewById(R.id.trend);
        focus=(TextView)findViewById(R.id.focus);
//        menu=(ImageView)findViewById(R.id.menu);
        trend_1=(LinearLayout)findViewById(R.id.trend_1);
        focus_1=(LinearLayout)findViewById(R.id.focus_1);
        view1=(View)findViewById(R.id.view1);
        view2=(View)findViewById(R.id.view2);
        trend_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                view1.setVisibility(View.VISIBLE);
                view2.setVisibility(View.GONE);
                trend.setTextColor(Color.parseColor("#af2126"));
                focus.setTextColor(Color.parseColor("#ff000000"));
            }
        });
        focus_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              view2.setVisibility(View.VISIBLE);
              view1.setVisibility(View.GONE);
                focus.setTextColor(Color.parseColor("#af2126"));
                trend.setTextColor(Color.parseColor("#ff000000"));

            }
        });
        add_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(IdeaListActivity.this,IdeasubmitActivity.class);
                startActivity(intent);
                finish();

            }
        });
//        menu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                drawer.isDrawerOpen(GravityCompat.START);
//            }
//        });
        IdeaList();



    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            // Handle the camera action
            Intent intent=new Intent(IdeaListActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
////        } else if (id == R.id.nav_share) {
////
////        } else if (id == R.id.nav_send) {
////
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void IdeaList(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        url = "http://demo.insonix.com/bush/get_suggestions_byid.php?user_id="+sharedpreferences.getString(uUid,"");
        Log.d("Url", "url" + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("res:", "resss" + jsonObject.toString());
                try{
                    String status=jsonObject.getString("status");
                    if(status.equalsIgnoreCase("true")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject catlist = jsonArray.getJSONObject(i);
                            String id = catlist.getString("id");
                            String title = catlist.getString("title");
                            String created_by = catlist.getString("created_by");
                            String likes = catlist.getString("likes");
                            String idea_des = catlist.getString("idea_des");
                           String file=catlist.getString("file_url1");

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", id);
                            hashMap.put("title", title);
                            hashMap.put("created_by", created_by);
                            hashMap.put("likes", likes);
                            hashMap.put("idea_des", idea_des);
                            hashMap.put("file_url1", file);
                            Log.d("filee",file);

                            addme.add(hashMap);
                        }
                        // addme = new ArrayList<>(addme);
//                        categoryAdapter = new CategoryAdapter(getActivity(), addme);
//                        taboneList.setAdapter(categoryAdapter);
                        ideaAdapter=new IdeaAdapter(IdeaListActivity.this,addme);
                        recyclerView.setAdapter(ideaAdapter);
                        //no_found.setVisibility(View.GONE);
                    }else{
                        new ShowMsg().createDialog(IdeaListActivity.this, "No IdeaList Found");
                        // no_found.setVisibility(View.VISIBLE);
                    }


                }catch(Exception e){
                    Log.d("eeee:", "eee" + e);
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError:","volleyError"+volleyError);
                progressDialog.dismiss();
            }


        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
    }
    class IdeaAdapter extends BaseAdapter{
        Activity activity;
        ArrayList<HashMap<String,String >>addme;
        public IdeaAdapter(Activity activity,ArrayList<HashMap<String,String >>addme){
            this.activity=activity;
            this.addme=addme;

        }
        @Override
        public int getCount() {
            return addme.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.idea_item,null);
            TextView desc=(TextView)convertView.findViewById(R.id.desc);
            TextView title=(TextView)convertView.findViewById(R.id.title);
            TextView like=(TextView)convertView.findViewById(R.id.like);
            ImageView profile_img=(ImageView)convertView.findViewById(R.id.profile_img);
            desc.setText(addme.get(position).get("idea_des"));
            title.setText(addme.get(position).get("title"));
            like.setText(addme.get(position).get("likes"));
            imageLoader.displayImage(addme.get(position).get("file_url1"),profile_img,displayImageOptions);
           /* Bitmap bitmap=decodeBase64(addme.get(position).get("file_url1"));
            profile_img.setImageBitmap(bitmap);*/

           //  profile_img.setImageBitmap(De);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(activity,DescriptionActivity.class);
                    intent.putExtra("title",addme.get(position).get("title"));
                    intent.putExtra("idea_des",addme.get(position).get("idea_des"));
                    intent.putExtra("id",addme.get(position).get("id"));

                    startActivity(intent);
                    finish();
                }
            });
            return convertView;
        }
    }
   /* public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }*/
}
