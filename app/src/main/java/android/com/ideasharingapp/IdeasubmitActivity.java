package android.com.ideasharingapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import iconstant.Iconstant;

/**
 * Created by insonix on 22/4/16.
 */
public class IdeasubmitActivity extends Activity implements Iconstant {
    TextView submit,edit_chooseidea,logistics_text,planning_text,tier1_text,tier2_text,lesscost_text,medcost_text,highcost_text;
    ImageView menu,image,image2;
//    ListView keylist;
    String list[]={"KPI-1","KPI-2","KPI-3","KPI-4","KPI-5","KPI-6","KPI-7","KPI-8","KPI-9","KPI-10"};
    Spinner key_spinner;
    String type;
    EditText title,first_name,last_name,description,result;
    String edit_chooseideas_text,title_text,first_text,last_text,description_text,costestimate_text,img;
    String url,file,profile_name1,cost_estimate,key_item;
    ProgressDialog progressDialog;
//    LinearLayout lesscostlay,medcostlay,highcostlay,tier1lay,tier2lay,logisticslay,planninglay;
    CheckBox lesscost,medcost,highcost,tier1,tier2,logistics,planning;
    Bitmap bitmap;
    String[] mTestArray;
    protected  static final int IMAGE_CAMERA_11=11;
    protected static final int IMAGE_GALLERY_22 = 22;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.submitactivity);
        menu=(ImageView)findViewById(R.id.menu);
        image=(ImageView)findViewById(R.id.image);
        image2=(ImageView)findViewById(R.id.image2);
        submit=(TextView)findViewById(R.id.submit);
//        keylist=(ListView)findViewById(R.id.keylist);
        key_spinner=(Spinner)findViewById(R.id.key_spinner);
        edit_chooseidea=(TextView)findViewById(R.id.edit_chooseidea);
        result=(EditText)findViewById(R.id.result);

        tier1=(CheckBox)findViewById(R.id.tier1);
        tier2=(CheckBox)findViewById(R.id.tier2);
        logistics=(CheckBox)findViewById(R.id.logistics);
        planning=(CheckBox)findViewById(R.id.planning);
        lesscost=(CheckBox)findViewById(R.id.lesscost);
        medcost=(CheckBox)findViewById(R.id.medcost);
        highcost=(CheckBox)findViewById(R.id.highcost);
        logistics_text=(TextView)findViewById(R.id.logistics_text);
        planning_text=(TextView)findViewById(R.id.planning_text);
        tier1_text=(TextView)findViewById(R.id.tier1_text);
        tier2_text=(TextView)findViewById(R.id.tier2_text);
        lesscost_text=(TextView)findViewById(R.id.lesscost_text);
        medcost_text=(TextView)findViewById(R.id.medcost_text);
        highcost_text=(TextView)findViewById(R.id.highcost_text);
        title=(EditText)findViewById(R.id.title);
        first_name=(EditText)findViewById(R.id.first_name);
        last_name=(EditText)findViewById(R.id.last_name);
        description=(EditText)findViewById(R.id.description);
//        cost_estimate=(EditText)findViewById(R.id.cost_estimate);
        progressDialog=new ProgressDialog(this);
        type = "Planning";
        cost_estimate="0 to 10k";
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//


//        ArrayAdapter arrayAdapter=new ArrayAdapter(IdeasubmitActivity.this,R.layout.chooseidearow,R.id.text1,list);
//        key_spinner.setAdapter(arrayAdapter);
        mTestArray = getResources().getStringArray(R.array.planning);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(IdeasubmitActivity.this,R.layout.support_simple_spinner_dropdown_item, mTestArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapter.notifyDataSetChanged();
        key_spinner.setAdapter(dataAdapter);
        planning.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    planning.setChecked(true);
                    logistics.setChecked(false);
                    tier1.setChecked(false);
                    tier2.setChecked(false);
                    type = "Planning";
                    logistics_text.setTextColor(Color.parseColor("#99000000"));
                planning_text.setTextColor(Color.parseColor("#fbc02d"));
                    tier1.setTextColor(Color.parseColor("#99000000"));
                tier2.setTextColor(Color.parseColor("#99000000"));
                    mTestArray = getResources().getStringArray(R.array.planning);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(IdeasubmitActivity.this,R.layout.support_simple_spinner_dropdown_item, mTestArray);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter.notifyDataSetChanged();
                    key_spinner.setAdapter(dataAdapter);
                }
            }
        });
//        logistics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//
//                    planning.setChecked(false);
//                    logistics.setChecked(true);
//                    tier1.setChecked(false);
//                    tier2.setChecked(false);
//                    type = "Logistics";
//                    logistics_text.setTextColor(Color.parseColor("#fbc02d"));
//                planning_text.setTextColor(Color.parseColor("#99000000"));
//                tier1.setTextColor(Color.parseColor("#99000000"));
//                tier2.setTextColor(Color.parseColor("#99000000"));
//                }
//            }
//        });
        tier1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    planning.setChecked(false);
                    logistics.setChecked(false);
                    tier1.setChecked(true);
                    tier2.setChecked(false);
                    type = "Tier |";
                    logistics_text.setTextColor(Color.parseColor("#99000000"));
                planning_text.setTextColor(Color.parseColor("#99000000"));
                tier1_text.setTextColor(Color.parseColor("#fbc02d"));
                tier2_text.setTextColor(Color.parseColor("#99000000"));
                    mTestArray = getResources().getStringArray(R.array.tier1);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(IdeasubmitActivity.this,R.layout.support_simple_spinner_dropdown_item, mTestArray);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter.notifyDataSetChanged();
                    key_spinner.setAdapter(dataAdapter);
                }
            }
        });
        tier2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    planning.setChecked(false);
                    logistics.setChecked(false);
                    tier1.setChecked(false);
                    tier2.setChecked(true);
                    type="Tier ||";
                    logistics_text.setTextColor(Color.parseColor("#99000000"));
                planning_text.setTextColor(Color.parseColor("#99000000"));
                tier1_text.setTextColor(Color.parseColor("#99000000"));
                tier2_text.setTextColor(Color.parseColor("#fbc02d"));
                    mTestArray = getResources().getStringArray(R.array.tier2);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(IdeasubmitActivity.this,R.layout.support_simple_spinner_dropdown_item, mTestArray);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter.notifyDataSetChanged();
                    key_spinner.setAdapter(dataAdapter);
                }
            }
        });
        lesscost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    medcost.setChecked(false);
                    highcost.setChecked(false);
                    lesscost.setChecked(true);
                    medcost_text.setTextColor(Color.parseColor("#99000000"));
                    highcost_text.setTextColor(Color.parseColor("#99000000"));
                    lesscost_text.setTextColor(Color.parseColor("#fbc02d"));
                    cost_estimate="0 to 10k";
                }
            }
        });
        medcost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    lesscost.setChecked(false);
                    highcost.setChecked(false);
                    medcost.setChecked(true);
                    lesscost_text.setTextColor(Color.parseColor("#99000000"));
                    highcost_text.setTextColor(Color.parseColor("#99000000"));
                    medcost_text.setTextColor(Color.parseColor("#fbc02d"));
                    cost_estimate="10k to 100k";
                }
            }
        });
        highcost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    medcost.setChecked(false);
                    lesscost.setChecked(false);
                    highcost.setChecked(true);
                    medcost_text.setTextColor(Color.parseColor("#99000000"));
                    lesscost_text.setTextColor(Color.parseColor("#99000000"));
                    highcost_text.setTextColor(Color.parseColor("#fbc02d"));
                    cost_estimate="Above 100k";
                }
            }
        });
        key_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                key_item = key_spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//
//
//
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             if(title.getText().toString().length() == 0){
                    title.setError("Title is required!");
                }
             else if(description.getText().toString().length() == 0){
                 description.setError("Description is required!");
             }
                else if(result.getText().toString().length()==0){
                 result.setError("Key result is required!");
             }
             else if(first_name.getText().toString().length()==0){
                 first_name.setError("First name is required!");
             }
             else if(last_name.getText().toString().length()==0){
                 last_name.setError("last name is required!");
             }
                else {
                 Log.d("type", "type" + type);
                 Log.d("cost_estimate", "cost_estimate" + cost_estimate);
                 Log.d("key_item", "key_item" + key_item);
                 json_object();
             }
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(IdeasubmitActivity.this,IdeaListActivity.class);
                startActivity(intent);
                finish();
          }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
selectImage();

            }
        });

    }
    void json_object(){
        url="http://demo.insonix.com/bush/add_suggestion.php?";
        progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("status", s);try {
                    JSONObject jsonObject = new JSONObject(s);
                    String status=jsonObject.getString("status");
                    String message=jsonObject.getString("message");
                    if(status.equalsIgnoreCase("true")) {
                        Toast.makeText(IdeasubmitActivity.this,message,Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(IdeasubmitActivity.this, IdeaListActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(IdeasubmitActivity.this,message,Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){

                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (bitmap == null) {

                } else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    byte data[] = byteArrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                    Log.d("imgdata", String.valueOf(data));
                }
                Log.d("type","type"+type);
                Log.d("cost_estimate","cost_estimate"+cost_estimate);
                params.put("title", title.getText().toString());
                params.put("type_of_idea", URLEncoder.encode(type));
                params.put("idea_des", description.getText().toString());
                params.put("cost_estimate", cost_estimate);
                params.put("firstname", first_name.getText().toString());
                params.put("kpi", key_item);
                params.put("results", result.getText().toString());
                params.put("lastname", last_name.getText().toString());
                params.put("user_id", sharedPreferences.getString(uUid,""));
                if (file == null) {

                } else {
                    params.put("profile_name1", "" + new Date().getTime() +".png");
                    params.put("photo1_data", file);
                }

                  return checkParams(params);

            }
            private Map<String, String> checkParams(Map<String, String> map){
                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
                    if(pairs.getValue()==null){
                        map.put(pairs.getKey(), "");
                    }
                }
                return map;
            }
        };


        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    private void selectImage() {



        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };



        AlertDialog.Builder builder = new AlertDialog.Builder(IdeasubmitActivity.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

//                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
//
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, IMAGE_CAMERA_11);

                }

                else if (options[item].equals("Choose from Gallery"))

                {

                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, IMAGE_GALLERY_22);



                }

                else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMAGE_CAMERA_11:
                if (data != null) {
                    Uri uri = data.getData();
                    try {
                        bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                        image2.setImageBitmap(bitmap);
                        image.setVisibility(View.GONE);
                        image2.setVisibility(View.VISIBLE);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
            case IMAGE_GALLERY_22:
                if (data != null) {
                    Uri uri = data.getData();
                    try {
                        bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                       image2.setImageBitmap(bitmap);
                        image.setVisibility(View.GONE);
                        image2.setVisibility(View.VISIBLE);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();


            Intent intent = new Intent(IdeasubmitActivity.this, IdeaListActivity.class);
            startActivity(intent);
            finish();
        }
    }

